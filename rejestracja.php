<?php
  session_start();


  if (isset($_SESSION['logged_in'])) {
    if($_SESSION['account_type'] == 0) header("Location: index.php");
    else header("Location: panel.php");
  }

  if (isset($_SESSION['logged_in']) && $_SESSION['account_type'] == 1) {
      $epic = true;
  }

 ?>

<!DOCTYPE html>
<html lang="pl">

<head>
  <?php require_once "parts/head.php"; ?>
</head>

<body>
  <div class="container-fluid" id="wrapper">

    <?php
    if(isset($_SESSION['reg_error']))
    {
      echo<<<HTML
      <div id="login-alert" class="alert-fade-index alert alert-danger alert-dismissible fade show" role="alert">
      $_SESSION[reg_error]
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
HTML;

      unset($_SESSION['reg_error']);
    }
      require_once "php_scripts/connect.php";
      require_once "parts/nav.php";
?>

    <article>

	<blockquote class="blockquote text-center">
  		<p class="mb-0">Those who know nothing of foreign languages know nothing of their own.</p>
  		<footer class="blockquote-footer">Johann Wolfgang von Goethe</footer>
	</blockquote>

  <div class="row">
    <div class="offset-md-2 col-md-8 col-xs-12">
        <?php
            if(isset($_SESSION['reg_login']))
            {
                $login = $_SESSION['reg_login'];
                $email = $_SESSION['reg_email'];
            }
            else {
                $login = "";
                $email = "";
            }

        echo<<<HTML

      <h4 class="mb-3">Rejestracja</h4>
      <form action="php_scripts/register.php" method="POST">
        <div class="col-md-12 mb-3">
          <label for="email">Nick</label>
          <input minlength="4" type="text" class="form-control" value="$login" placeholder="Pseudonim" name="nick" required>
        </div>
        <div class="col-md-12 mb-3">
          <label for="email">E-mail</label>
          <input type="email" class="form-control" value="$email" placeholder="Adres e-mail" name="email" required>
        </div>
        <div class="row p30">
          <div class="col-md-6 mb-3">
            <label for="haslo">Hasło</label>
            <input minlength="8" type="password" class="form-control" value="" placeholder="hasło" name="haslo" required>
          </div>

          <div class="col-md-6 mb-3">
            <label for="phaslo">Powtórz hasło</label>
            <input minlength="8" type="password" class="form-control" value="" placeholder="hasło" value="" name="phaslo" required>
          </div>
          <input class="m30 btn btn-violet form-control" type="submit" value="Zarejestruj się!">
        </div>

      </form>
HTML;
?>

    </article>
    <footer>
      <?php require_once "parts/footer.php"; ?>
    </footer>
  </div>
<?php $connection->close(); ?>
</body>
</html>
