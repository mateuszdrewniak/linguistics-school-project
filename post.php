<?php

  session_start();

  if (isset($_SESSION['logged_in']) && $_SESSION['account_type'] == 1) {
      $epic = true;
  }

  if (!isset($_GET['post'])) {
    header("Location: index.php");
    exit();
  }

  require_once "php_scripts/connect.php";

  if ($connection->connect_errno != 0) {
    echo "Problem z połączeniem z bazą danych!";
  }
  else {
    $post_id = $_GET['post'];
    $postQuery1 = "SELECT * FROM posts JOIN categories ON posts.cat_id = categories.cat_id WHERE post_id = $post_id";
    $result = $connection->query($postQuery1);
    if ($result->num_rows < 1) {
      header("Location: index.php");
    }
    else {
      $row = $result->fetch_assoc();
      $catName = $row['cat_name'];
      $title = $row['title'];
      $content = $row['content'];

      $postQuery2 = "SELECT * FROM images WHERE post_id = $post_id";
      $result = $connection->query($postQuery2);
      $row = $result->fetch_assoc();
      $img = $row['img_name'];
      $result->free_result();
    }
  }

  if (isset($_SESSION['logged_in'])) {
    $disabled = "";
    $placeholder = "Komentarz";
  }
  else {
    $disabled = "disabled";
    $placeholder = "Zaloguj się, aby dodać komentarz!";
  }


 ?>

<!DOCTYPE html>
<html lang="pl">

<head>
  <?php require_once "parts/head.php"; ?>
</head>

<body>
  <div class="container-fluid" id="wrapper">

<?php require_once "parts/nav.php";

    if(isset($_SESSION['comment_log']))
    {
      echo<<<HTML
      <div id="login-alert" class="alert-fade-index alert alert-success alert-dismissible fade show" role="alert">
      $_SESSION[comment_log]
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
HTML;

      unset($_SESSION['comment_log']);
    }

  echo<<<HTML
  <article>

  <img class="img-fluid img-post" src="$img" alt="Linguistic post">

  <h2 class="post-header">$title</h2>

  <div class="post_body">

    <h3><div class="category">$catName</div></h3>

    <span class="separator"></span>

    $content

  </div>
  <hr>

HTML;

if ($connection->connect_errno != 0) {
  echo "Problem z połączeniem z bazą danych!";
}
else {

  $commentQuery = "SELECT users.user_id, comments.comment_id, nick, comments.date, comment
    FROM comments
    JOIN users ON comments.user_id = users.user_id
    JOIN posts ON posts.post_id = comments.post_id
    WHERE posts.post_id = $post_id;";
  $result = $connection->query($commentQuery);
  $num_rows = $result->num_rows;

  if($num_rows == 0 || $num_rows > 4) $comQ = "Komentarzy";
  elseif($num_rows == 1) $comQ = "Komentarz";
  elseif($num_rows > 1 && $num_rows < 5) $comQ = "Komentarze";
  else $comQ = "Komentarzy";

  echo<<<HTML
  <div class="comment-section">
    <h4 class="text-center">$num_rows $comQ:</h4>
HTML;

  while ($row = $result->fetch_assoc()) {
    $comment_id = $row['comment_id'];
    $user_id = $row['user_id'];
    $comment = $row['comment'];
    $rawDate = $row['date'];
    $nick = $row['nick'];
    if (isset($_SESSION['user_id']) && $user_id == $_SESSION['user_id']) {
      $delete = "<a title=\"Usuń komentarz\" href=\"php_scripts/del_comment.php?comment=$comment_id&post=$post_id\"<i class=\"del-com fas fa-times-circle\"></i></a>";
    }
    elseif(isset($epic)){
      $delete = "<a title=\"Usuń komentarz\" href=\"php_scripts/del_comment.php?comment=$comment_id&post=$post_id\"<i class=\"del-com fas fa-times-circle\"></i></a>";
    }
    else $delete = "";
    $month = date("n", strtotime($row['date'])); // liczbowa forma miesiąca od 1 do 12

    switch ($month) {
      case 1:
        $month = "Stycznia"; break;
      case 2:
        $month = "Lutego"; break;
      case 3:
        $month = "Marca"; break;
      case 4:
        $month = "Kwietnia"; break;
      case 5:
        $month = "Maja"; break;
      case 6:
        $month = "Czerwca"; break;
      case 7:
        $month = "Lipca"; break;
      case 8:
        $month = "Sierpnia"; break;
      case 9:
        $month = "Września"; break;
      case 10:
        $month = "Października"; break;
      case 11:
        $month = "Listopada"; break;
      case 12:
        $month = "Grudnia"; break;
    }

    $day = date("j", strtotime($row['date']));
    $yearHours = date("Y; h:i", strtotime($row['date']));

    echo<<<HTML
    <div class="comment">
      <h5>$nick $delete</h5>
      <div class="text-muted">$day $month, $yearHours</div>
      <p>$comment</p>
    </div>
HTML;


  }
  echo<<<HTML
  </div>

  <hr>
HTML;


}






echo<<<HTML

  <form action="php_scripts/add_comment.php" method="POST">
      <div class="form-group">
        <textarea minlength="10" $disabled class="form-control mb-10" rows="5" name="comment" placeholder="$placeholder" required=""></textarea>
        <input type="hidden" name="post_id" value="$post_id">
      </div>
      <input type="submit" class="btn btn-primary" $disabled value="Opublikuj Komentarz">

  </form>

</article>
HTML;
?>




    <footer>
      <?php require_once "parts/footer.php"; ?>
    </footer>
  </div>

</body>
</html>
