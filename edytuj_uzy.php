<?php

  session_start();

  if(!isset($_SESSION['logged_in']) || $_SESSION['account_type'] != 1){
    header('Location: index.php');
  }

  if(!isset($_GET['user'])){
      header('Location: panel.php');
  }
  else {
      require_once "php_scripts/connect.php";

      if ($connection->connect_errno != 0) {
          echo "Błąd połączenia!";
      }
      else {
          $user_id = $_GET['user'];
          $query = "SELECT * FROM users WHERE user_id = $user_id";
          $result = $connection->query($query);
          if($result->num_rows < 1){
              header("Location: panel.php");
          }
          else {
              $row = $result->fetch_assoc();
              $nick = $row['nick'];
              $email = $row['email'];
          }
      }
  }

 ?>

<!DOCTYPE html>
<html lang="pl">

<head>
  <?php require_once "parts/head.php"; ?>
</head>

<body>
  <div class="container-fluid" id="wrapper">

    <?php
    if(isset($_SESSION['user_change']))
    {
      echo<<<HTML
      <div id="login-alert" class="alert-fade-index alert alert-success alert-dismissible fade show" role="alert">
      $_SESSION[user_change]
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
HTML;

      unset($_SESSION['user_change']);
    }

    if(isset($_SESSION['add_cat']))
    {
      if(!isset($_SESSION['add_cat_success'])) $alert_color = ' alert-danger ';
      else{ $alert_color = ' alert-success '; unset($_SESSION['add_cat_success']);}
      echo<<<HTML
      <div id="login-alert" class="alert-fade-index alert $alert_color alert-dismissible fade show" role="alert">
      $_SESSION[add_cat]
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
HTML;

      unset($_SESSION['add_cat']);
    }


    require_once "parts/panel_nav.php";


  ?>



    <main>
      <h1>Edytuj użytkownika</h1>


      <?php

        echo<<<HTML
        <form method="POST" action="php_scripts/edit_user.php" style="width: 80%; margin: 0 auto 0 auto;">
          <div class="form-group">
           <input minlength="5" class="form-control mb-10" type="text" name="nick" value="$nick" placeholder="Nick" required>
           <input type="hidden" name="user_id" value="$user_id">
          </div>
       <div class="form-group">
            <input minlength="5" class="form-control mb-10" rows="5" name="email" value="$email" placeholder="Email" required="">
          </div>
          <input type="submit" class="btn btn-primary" value="Zmień dane">
    </form>
HTML;


       ?>

    </main>
  </div>
  <?php
  echo '<div class="modal fade" id="add-cat-mod" tabindex="-1" role="dialog" aria-labelledby="add-cat-mod-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form method="POST" action="php_scripts/add_cat.php">
          <div class="modal-content">
            <div class="modal-header">

            <h4 class="modal-title" id="add-cat-mod-label"><span class="msg-del-mod-header1">Dodawanie kategorii</span>.</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">';
            if(isset($_SESSION['add_cat_mod']))
            {
              echo '<div class="alert alert-fade alert-danger alert-dismissible fade show" role="alert">
              '.$_SESSION['add_cat_mod'].'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';

            }
              echo '<div class="form-group" style="max-width: 100%;">
                  <label for="category" class="col-form-label"><h6 class="emphasis-red bold-text"></h6></label>
                  <input type="text" class="form-control" name="category" id="category" placeholder="Wpisz nazwę kategorii" required>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-orange" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-success clickable-del">Dodaj</button>
            </div>
          </div>
        </form>
        </div>
      </div>';

      echo '<div class="modal fade" id="del-cat-mod" tabindex="-1" role="dialog" aria-labelledby="del-cat-mod-label" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <form method="POST" action="php_scripts/del_cat.php">
              <div class="modal-content">
                <div class="modal-header">

                <h4 class="modal-title" id="del-cat-mod-label"><span class="msg-del-mod-header1">Usuwanie kategorii</span>.</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">';
                if(isset($_SESSION['del_cat_mod']))
                {
                  echo '<div class="alert alert-fade alert-danger alert-dismissible fade show" role="alert">
                  '.$_SESSION['del_cat_mod'].'
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>';

                }
                  echo '<div class="form-group" style="max-width: 100%;">
                      <select class="form-control" name="del_cat" required>';
                      $query = "SELECT * FROM categories;";
                      $result = $connection->query($query);
                      while($row = $result->fetch_assoc()){
                        echo<<<HTML
                        <option value="$row[cat_id]">$row[cat_name]</option>
HTML;

                      }
                    echo  '</select>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-orange" data-dismiss="modal">Zamknij</button>
                  <button type="submit" class="btn btn-danger clickable-del">Usuń</button>
                </div>
              </div>
            </form>
            </div>
          </div>';

  $connection->close();
   ?>
  <script type="text/javascript">
    <?php
      if(isset($_SESSION['add_cat_mod'])){ echo '$(\'#add-cat-mod\').modal(\'show\');'; unset($_SESSION['add_cat_mod']);}
      if(isset($_SESSION['del_cat_mod'])){ echo '$(\'#del-cat-mod\').modal(\'show\');'; unset($_SESSION['del_cat_mod']);}
    ?>
  </script>

</body>
</html>
