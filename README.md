PHP Linguistics website
========================

How to take a look at this website?
----------------------------
- Clone or download this repository
- install XAMPP or MAMP
- In case of using XAMPP you need to alter it's configuration to make it work on that site or you can clone the repo directly into the `htdocs` folder within the main XAMPP directory
- Open XAMPP or MAMP and turn on `Apache server` and `MySQL server`
- Open your browser and enter `localhost/phpmyadmin` in the URL field
- Enter the `Import` menu
- Under the `File to import` section, use the file input and browse for the `lingwistyka.sql` file inside this repository
- The database should now be imported, and the site ready to go
- Now you can enter `localhost` in the URL field of your browser and see this website