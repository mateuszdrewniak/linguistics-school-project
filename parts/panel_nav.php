<?php
echo<<<HTML
<aside class="side-nav">
  <p class="logo"><a href="panel.php">/Lıŋˈgwıstıks/</a></p>
  <div class="spearator"></div>
  <nav>
    <a data-toggle="modal" data-target="#add-cat-mod">
      <div class="side-nav-item">
        <i class="fas fa-plus-square"></i>
        <p>Dodaj kategorię</p>
      </div>
    </a>
    <a data-toggle="modal" data-target="#del-cat-mod">
      <div class="side-nav-item">
        <i class="fas fa-times-circle"></i>
        <p>Usuń kategorię</p>
      </div>
    </a>
    <a href="index.php">
      <div class="side-nav-item">
        <i class="fas fa-home"></i>
        <p>Główna strona</p>
      </div>
    </a>
    <a href="użytkownicy.php">
      <div class="side-nav-item">
        <i class="fas fa-users"></i>
        <p>Użytkownicy</p>
      </div>
    </a>
  </nav>
  <a style="display: block;" class="btn btn-light btn-logout" href="php_scripts/logout.php">Wyloguj się</a>
</aside>
HTML;
?>
