<?php
echo<<<HTML
  <div class="inner-footer">
  <div class="row">
   <div class="col-xs-12 col-sm-4 text-center-custom">
     <p class="logo">/Lıŋˈgwıstıks/</p>
     <div class="features">
       <h3>Cechy strony</h3>
       <p>Witryna stworzona jako projekt szkolny. Logo strony (w kilku wariantach) zostało zaprojektowane przeze mnie. Strona korzysta z <i class="css">CSS3</i>, zbudowana jest na znacznikach <i class="html">HTML5</i> oraz <i class="bootstrap">Bootstrapie</i>. Jest <i>responsywna</i>, lekka i wykorzystuje zróżnicowane środki.</p>
     </div>

   </div>
   <div class="col-xs-12 col-sm-4 text-center-custom">
     <ul>
       <li><h5>Główne linki</h5>
         <ol>
           <li><a href="index.php">Strona główna</a></li>
           <li><a href="rejestracja.php">Rejestracja</a></li>
         </ol>
       </li>
       <li><h5>Treść merytoryczna</h5>
         <ol>
           <li><a href="rodziny.html">Clarke</a></li>
           <li><a href="budowa.html">Kent</a></li>
         </ol>
       </li>
     </ul>
   </div>
   <div class="col-xs-12 col-sm-4 text-center-custom">
     <img class="author-photo" src="assets/img/Matthias.jpg" alt="Mateusz">
     <span class="author">
       <h3>Mateusz Drewniak</h3>
       <p>Autor strony o lingwistyce. Miłośnik niekonwencjonalnych rozwiązań, <i>historii</i> wczesnego średniowiecza, <span class="rule_britannia"><i class="b">kultury</i> <i class="w">anglo</i><i class="r">saskiej</i></span> i fantastyki.</p>
   </span>
   </div>
  </div>
  </div>

  <!-- Autorstwa Mateusza Patryka Wawrzyńca Drewniaka, jako projekt zaliczeniowy na zajęcia Pracowni Witryn i Aplikacji w IV klasie Technikum Inforamtycznego w Zespole Szkół Komunikacji im. Hipolita Cegielskiego w Poznaniu. Rocznik 1999. -->
HTML;
?>
