<?php
echo<<<HTML
<meta charset="utf-8"/>
 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
 <meta name="viewport" content="width=device-width, initial-scale=1"/>
 <title>Lingwistyka | Blog</title>
 <meta name="keywords" content="procesory, x86">
 <meta name="description" content="Informatywna strona o procesorach. Wiedza o technologii w pigułce!">
 <meta name="author" content="Mateusz Drewniak">

 <link href="assets/css/style.css" rel="stylesheet">
 <link href="assets/css/colours-boyish.css" rel="stylesheet">
 <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
 <link href="https://fonts.googleapis.com/css?family=Barlow:400,400i|Federant|Quicksand&amp;subset=latin-ext" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Fondamento&amp;subset=latin-ext" rel="stylesheet">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

 <script type="text/javascript" src="assets/js/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 <link rel="icon" type="image/png" href="assets/img/favico.png" />
 <style>
   a, a:hover{text-decoration: none;}
   nav ol li a.dropdown-menu{color: #111E1C !important;}
 </style>
 <script type="text/javascript">
   window.setTimeout(function() {
     $(".alert-fade-index").fadeTo(400, 0).slideUp(400, function(){
         $(this).remove();
      });
    }, 4000);
    window.setTimeout(function() {
        $(".alert-fade").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
 </script>
HTML;
?>
