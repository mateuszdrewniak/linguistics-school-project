<?php

  function random_string($length)
  {
    $alphanum = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
    $last_char = (strlen($alphanum))-1;
    $output = "";

    for($i=1; $i<=$length; $i++)
    {
      $char = rand(0, $last_char);
      $output.=substr($alphanum, $char, 1);

    }
    return $output;
  }

  function random_int_mine($length)
  {
    $alphanum = '1234567890';
    $last_char = (strlen($alphanum))-1;
    $final_string = "";
    $zero = 0;

    for($i=1; $i<=$length; $i++)
    {
      if($i == 1) $char = rand(0, $last_char-1);
      else $char = rand(0, $last_char);

      $final_string.=substr($alphanum, $char, 1);
    }
    $final_int = (int)$final_string;
    return $final_int;
  }

  function utf8_substr_replace($original, $replacement, $position, $length)
  {
      $startString = mb_substr($original, 0, $position, "UTF-8");
      $endString = mb_substr($original, $position + $length, mb_strlen($original), "UTF-8");

      $out = $startString . $replacement . $endString;

      return $out;
  }

?>