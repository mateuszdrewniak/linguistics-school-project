<?php
session_start();
  if (isset($_SESSION['logged_in'])) {
    if ($_SESSION['account_type'] == 1) {
      header("Location: ../panel.php");
    }
    else {
      header("Location: ../index.php");
    }
  }
  else {
    if (isset($_POST['login'])) {
      require_once "connect.php";
      $login = $connection->real_escape_string($_POST['login']);
      $pass = $connection->real_escape_string($_POST['pass']);

      if ($connection->connect_errno == 0) {
        $loginQuery = "SELECT * FROM users WHERE nick = \"$login\";";

        $result = $connection->query($loginQuery);

        if ($result->num_rows > 0) {
          $row = $result->fetch_assoc();
          $realPass = $row['pass'];

          if (password_verify($pass, $realPass)) {

            if ($row['active'] == 0){
              $_SESSION['login_error'] = "Twoje konto nie jest aktywne!";
              header("Location: ../index.php");
            }
            else {
              if ($row['type'] == 0) {
                $_SESSION['user_id'] = $row['user_id'];
                $_SESSION['nick'] = $login;
                $_SESSION['email'] = $row['email'];
                $_SESSION['active'] = $row['active'];
                $_SESSION['account_type'] = $row['type'];
                $_SESSION['logged_in'] = true;
                $_SESSION['login_error_success'] = true;
                $_SESSION['login_error'] = "<b>Witaj $login!</b>";
                header("Location: ../index.php");

              }
              else {
                $_SESSION['user_id'] = $row['user_id'];
                $_SESSION['nick'] = $login;
                $_SESSION['email'] = $row['email'];
                $_SESSION['active'] = $row['active'];
                $_SESSION['account_type'] = $row['type'];
                $_SESSION['logged_in'] = true;
                $_SESSION['login_error_success'] = true;
                header("Location: ../panel.php");
              }

            }
          }
          else {

            $_SESSION['login_error'] = "Niepoprawny login lub hasło!";
            header("Location: ../index.php");
          }
        }
        else {
          echo "chuj";
          $_SESSION['login_error'] = "Niepoprawny login lub hasło!";
          header("Location: ../index.php");
        }
      }
      else {
        header("Location: ../index.php");
      }
      $connection->close();
    }
    else {
      header("Location: ../index.php");
    }
  }



 ?>
