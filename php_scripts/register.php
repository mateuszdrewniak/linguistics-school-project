<?php
session_start();
  if (isset($_SESSION['logged_in'])) {
    if ($_SESSION['account_type'] == 1) {
      header("Location: ../panel.php");
    }
    else {
      header("Location: ../index.php");
    }
  }
  else {
    if (isset($_POST['nick'])) {
      require_once "connect.php";
      require_once "functions.php";

      if ($connection->connect_errno == 0) {
        $login = $connection->real_escape_string($_POST['nick']);
        $email = $connection->real_escape_string($_POST['email']);
        $pass1 = $connection->real_escape_string($_POST['haslo']);
        $pass2 = $connection->real_escape_string($_POST['phaslo']);

        $_SESSION['reg_login'] = $login;
        $_SESSION['reg_email'] = $email;
        if ($pass1 != $pass2) {

          $_SESSION['reg_error'] = "Hasła są różne!";
          header("Location: ../rejestracja.php");
        }
        elseif (strlen($pass1) < 8) {
          $_SESSION['reg_error'] = "Hasło jest za krótkie! (min. 8 znaków)";
          header("Location: ../rejestracja.php");
        }
        else {
          $emailQuery = "SELECT * FROM users WHERE email = \"$email\";";
          $result = $connection->query($emailQuery);
          if ($result->num_rows > 0) {
            $_SESSION['reg_error'] = "Email jest zajęty!";
            header("Location: ../rejestracja.php");
          }
          else {
            $nickQuery = "SELECT * FROM users WHERE nick = \"$nick\";";
            $result = $connection->query($nickQuery);
            if ($result->num_rows > 0) {
              $_SESSION['reg_error'] = "Nick jest zajęty!";
              header("Location: ../rejestracja.php");
            }
            else {
              $pass_hash = password_hash($pass1, PASSWORD_DEFAULT);
              $act_code = random_int_mine(6);
              $insertQuery = "INSERT INTO users VALUES(NULL, '$login', '$email', '$pass_hash', $act_code, 1, 0);";
              $rezultat = $connection->query($insertQuery);
              $_SESSION['login_error'] = "Konto <b>$login</b> zarejestrowane!";
              $_SESSION['login_error_success'] = true;
              unset($_SESSION['reg_login']);
              unset($_SESSION['reg_email']);
              header("Location: ../index.php");
            }
          }
        }
      }


      $connection->close();
    }
    else {
      header("Location: ../rejestracja.php");
    }
  }




 ?>
