<?php
  session_start();

  if (!isset($_POST['category'])) {
    header("Location: ../index.php");
  }
  else {
    require_once "connect.php";
    if ($connection->connect_errno != 0) {
      header("Location: ../index.php");
    }
    else {
      $category = $connection->real_escape_string($_POST['category']);
      $selectQuery = "SELECT * FROM categories WHERE cat_name = '$category'";
      $result = $connection->query($selectQuery);
      if ($result->num_rows > 0) {
        $_SESSION['add_cat_mod'] = "Kategoria już istnieje!";
        header("Location: ../panel.php");
      }
      else {
        $insertQuery = "INSERT INTO categories VALUES(NULL, '$category');";
        $result = $connection->query($insertQuery);
        $_SESSION['add_cat'] = "Kategoria <b>$category</b> została dodana!";
        $_SESSION['add_cat_success'] = true;
        header("Location: ../panel.php");
      }

    }


    $connection->close();
  }


 ?>
